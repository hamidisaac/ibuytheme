$(document).ready(function() {
    if($("#address-map").length !== 0) {
        var app = new Mapp({
            element: '#address-map',
            presets: {
                latlng: {
                    lat: 35.74160794681015,
                    lng: 51.40145868994296,
                },
                zoom: 11,
            },
            apiKey: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjliMzkxNDk4YzljMmJjZDQyMTA0MzFlYTY1N2E2ZDgxZDlhNmM1NDE4M2VmOWYzZjVjOWNhMzM3ODA5ZDNlZjgwZjM3ODYzNjI2Y2U5ODg4In0.eyJhdWQiOiIxMDA2MiIsImp0aSI6IjliMzkxNDk4YzljMmJjZDQyMTA0MzFlYTY1N2E2ZDgxZDlhNmM1NDE4M2VmOWYzZjVjOWNhMzM3ODA5ZDNlZjgwZjM3ODYzNjI2Y2U5ODg4IiwiaWF0IjoxNTk0NjI2MjY4LCJuYmYiOjE1OTQ2MjYyNjgsImV4cCI6MTU5NzIxODI2OCwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.nvbiRZjiUotB_eXIMN4EKzMLdCyOop4OcSsoj04NlGiVI2S4ywrt5P4PXinl7PYHjwqdGHII3lnqYYCzHU9EgUez3d2jEEl5aeiCTOCW2XMb6-8ZGXzCtT3uExuc7mWynbAL4QscrXaYnXNb1ZRSeuQYGtZ_doE0x1WH9AC0n3hds4gwBhmjMyRiuB_GrQvHbqDI9VgmPPl0OIlayANnwclVFqs8jDEseLJTykTRQY0t057Vg9NybT8_qa_FQr5UWQvrZqQtE5E6cnVXy2GGNQp7wXuOVFYtxJGnHmXIEQEUF1sfNcdqZn1YhLCRPNaepzXjRV9YibAEQeaaCItGYg'
        });
        app.addLayers();

        app.addVectorLayers();
        // Add in a crosshair for the map

        var crosshairIcon = L.icon({
            iconUrl: prestashop.urls.theme_assets +'images/pin-location.png',
            iconSize:     [40, 40], // size of the icon
            iconAnchor:   [40, 40], // point of the icon which will correspond to marker's location
        });
        let crosshairMarker = new L.marker(app.map.getCenter(), {icon: crosshairIcon, clickable: false});
        crosshairMarker.addTo(app.map);
        // Move the crosshair to the center of the map when the user pans
        $('input[name = map_lat]').val(app.map.getCenter().lat);
        $('input[name = map_lng]').val(app.map.getCenter().lng);
        app.map.on('move', function(e) {
            crosshairMarker.setLatLng(app.map.getCenter());
            $('input[name = map_lat]').val(app.map.getCenter().lat);
            $('input[name = map_lng]').val(app.map.getCenter().lng);
        });
        crosshairMarker.on('click', function(event){
            console.log(event.latlng)
        });
    }
});
