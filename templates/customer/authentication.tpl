
<!doctype html>
<html lang="{$language.iso_code}" style="height: 100%">

<head>
  {block name='head'}
    {include file='_partials/head.tpl'}
  {/block}
</head>

<body id="{$page.page_name}" class="{$page.body_classes|classnames}">
{block name='page_content'}
  {block name='login_form_container'}
    <section class="login-form">
      {render file='customer/_partials/login-form.tpl' ui=$login_form}
    </section>
    {block name='display_after_login_form'}
      {hook h='displayCustomerLoginFormAfter'}
    {/block}

  {/block}
{/block}

{block name='javascript_bottom'}
  {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
{/block}

{block name='hook_before_body_closing_tag'}
  {hook h='displayBeforeBodyClosingTag'}
{/block}

</body>

</html>



