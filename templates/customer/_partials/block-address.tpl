
{block name='address_block_item'}
  <article id="address-{$address.id}" class="address-item" data-id-address="{$address.id}">
    <label for="address-item-{$address.id}" class="address-item-body pr-3">
      <strong>{$address.alias}</strong>
      {$address.city nofilter}, {$address.address1 nofilter}
    </label>

    {block name='address_block_item_actions'}
      <div class="address-item-options">
        <a
                class="edit-address "
                data-link-action="edit-address"
                href="{url entity=address id=$address.id}"
        >
          {l s='Update' d='Shop.Theme.Actions'}
        </a>
        <a
                class="delete-address "
                data-link-action="delete-address"
                href="{url entity=address id=$address.id params=['delete' => 1, 'token' => $token]}"
        >
          {l s='Delete' d='Shop.Theme.Actions'}
        </a>
      </div>
      
    {/block}
  </article>
{/block}
