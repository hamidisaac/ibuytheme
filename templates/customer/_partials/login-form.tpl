{block name='login_form'}

  {block name='login_form_errors'}
    {include file='_partials/form-errors.tpl' errors=$errors['']}
  {/block}

  <form id="login-form" action="{block name='login_form_actionurl'}{$action}{/block}" method="post">
    <section>
      <div class="login-log"></div>
      {block name='login_form_fields'}
        {block "form_fields"}
          <div class="send-sms-box"  >
            {block "form_field"}
              {if isset($formFields['phone']) }
                {form_field field=$formFields['phone'] style='form-group'}
              {/if}
            {/block}
            <div class="note">برای ورود شماره موبایل خود را وارد نمایید، یک کد 5 رقمی از طریق پیامک برای شما ارسال خواهد شد</div>
          </div>
          <div class="verification-box" style="display: none">
            {foreach from=$formFields item="field"}
              <div id="form_input_{$field['name']}">
                {block "form_field"}
                  {if  $field['name'] !== 'phone'}
                    {form_field field=$field style='form-group'}
                  {/if}
                {/block}
              </div>
            {/foreach}
          </div>
        {/block}
      {/block}
    </section>

    {block name='login_form_footer'}

        <input type="hidden" name="submitLogin" value="1">
        {block name='form_buttons'}
          <button id="submit-login" class="btn form-control-submit send-sms-btn" data-link-action="sign-in" type="submit" class="form-control-submit">
            {l s='Sign in' d='Shop.Theme.Actions'}
          </button>
        {/block}

    {/block}

  </form>
{/block}
