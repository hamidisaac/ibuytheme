
{block name='customer_form'}
  {block name='customer_form_errors'}
    {include file='_partials/form-errors.tpl' errors=$errors['']}
  {/block}

<form action="{block name='customer_form_actionurl'}{$action}{/block}" id="customer-form" class="js-customer-form" method="post">
    <div class="login-log"></div>
    {block "form_fields"}
        {foreach from=$formFields item="field"}
            <div id="form_input_{$field['name']}">
              {block "form_field"}
                {if  $field['name'] !== 'code'}
                  {form_field field=$field style='form-group'}
                {/if}
              {/block}
            </div>
        {/foreach}
      {$hook_create_account_form nofilter}
    {/block}


  {block name='customer_form_footer'}

      <input type="hidden" name="submitCreate" value="1">
      {block "form_buttons"}
          <button class="btn btn-primary form-control-submit" data-link-action="save-customer" type="submit">
              {l s='ذخیره' d='Shop.Theme.Actions'}
          </button>
      {/block}
  {/block}

</form>
{/block}
