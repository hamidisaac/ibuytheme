<!doctype html>
<html lang="{$language.iso_code}">

<head>
    {block name='head'}
        {include file='_partials/head.tpl'}
    {/block}
</head>

<body id="{$page.page_name}" class="{$page.body_classes|classnames}">

{block name='page_content'}
    {block name='register_form_container'}
        {$hook_create_account_top nofilter}
        <section class="register-form">
            {render file='customer/_partials/customer-form.tpl' ui=$register_form}
        </section>
    {/block}
{/block}

{block name='javascript_bottom'}
    {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
{/block}

{block name='hook_before_body_closing_tag'}
    {hook h='displayBeforeBodyClosingTag'}
{/block}

</body>

</html>




