
{extends file='customer/page.tpl'}

{block name='page_content'}
  <div class="address-form">
    {render template="customer/_partials/address-form.tpl" ui=$address_form}
  </div>
{/block}
