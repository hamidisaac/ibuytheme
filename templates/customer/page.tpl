{extends file='page.tpl'}

{block name='notifications'}{/block}
{block name='top_block'}{/block}

{block name='page_content_container'}
    <section id="content" class="page-content user-profile">

        <div class="row">
            <div class="col-4">
                <div class="card right-box">
                    <div class="user-info">
                        <div class="image">
                            <div class="avatar"></div>
                        </div>
                        <div class="info">
                            <h3>حمید سلیمانی</h3>
                            <div class="user-phone">09392042250</div>
                        </div>
                    </div>
                    <ul class="links">
                        <li class="item">
                            <a id="identity-link" href="{$urls.pages.identity}">
                                {l s='Information' d='Shop.Theme.Customeraccount'}
                            </a>
                        </li>
                        <li class="item">
                            <a id="addresses-link" href="{$urls.pages.addresses}">
                                {l s='Addresses' d='Shop.Theme.Customeraccount'}
                            </a>
                        </li>
                        {if !$configuration.is_catalog}
                            <li class="item">
                                <a id="history-link" href="{$urls.pages.history}">
                                    {l s='Order history and details' d='Shop.Theme.Customeraccount'}
                                </a>
                            </li>

                        {/if}
                        {if $configuration.voucher_enabled && !$configuration.is_catalog}
                            <li class="item">
                                <a id="discounts-link" href="{$urls.pages.discount}">
                                    {l s='Vouchers' d='Shop.Theme.Customeraccount'}
                                </a>
                            </li>
                        {/if}
                        {if $configuration.return_enabled && !$configuration.is_catalog}
                            <li class="item">
                                <a id="returns-link" href="{$urls.pages.order_follow}">
                                    {l s='Merchandise returns' d='Shop.Theme.Customeraccount'}
                                </a>
                            </li>

                        {/if}

{*                        {block name='display_customer_account'}*}
{*                            {hook h='displayCustomerAccount'}*}
{*                        {/block}*}
                        <li class="item">
                            <a href="{$logout_url}" >
                                {l s='Sign out' d='Shop.Theme.Actions'}
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="col-8">
                <div class="card left-box">
                    {block name='page_content_top'}
                       <div class="">
                           {block name='customer_notifications'}
                               {include file='_partials/notifications.tpl'}
                           {/block}
                       </div>
                    {/block}
                    <div class="card-body">
                        {block name='page_content'}

                        {/block}
                    </div>
                </div>
            </div>
        </div>
    </section>
{/block}

{block name='page_footer'}

{/block}
