{extends file='customer/page.tpl'}

{block name='page_content'}
  <div class="address-selector">
    <h2 class="title">آدرس های شما</h2>
  {foreach $customer.addresses as $address}
    {block name='customer_address'}
      {include file='customer/_partials/block-address.tpl' address=$address}
    {/block}
  {/foreach}
  </div>
  <div class="clearfix"></div>
  <div class="addresses-footer">
    <a href="{$urls.pages.address}" class="btn btn-primary" data-link-action="add-address">
      <span>{l s='Create new address' d='Shop.Theme.Actions'}</span>
    </a>
  </div>
{/block}
