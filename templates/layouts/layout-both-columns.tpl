<!doctype html>
<html lang="{$language.iso_code}">

<head>
    {block name='head'}
        {include file='_partials/head.tpl'}
    {/block}
</head>

<body id="{$page.page_name}" class="{$page.body_classes|classnames}">

{block name='hook_after_body_opening_tag'}
    {hook h='displayAfterBodyOpeningTag'}
{/block}

<main>
    <!-- need more working -->
    {block name='product_activation'}
        {include file='catalog/_partials/product-activation.tpl'}
    {/block}

    <header id="header" class="main-header">
        {block name='header'}
            {include file='_partials/header.tpl'}
        {/block}
    </header>

    <!-- need more working -->
    {block name='notifications'}
        {include file='_partials/notifications.tpl'}
    {/block}

    {if $page.page_name == 'index'}
        {include file='_partials/homepage.tpl'}
    {else}
        {block name="top_block"}
        <div class="top-box">
            <div class="container">
                    {if $page.page_name == 'product'}
                        {hook h='displayTopBlockProduct'}
                    {else}
                        {hook h='displayTopBlock'}
                    {/if}
            </div>
        </div>
        {/block}
        <div class="content">
            {hook h="displayWrapperTop"}
            <div class="container">
                <div class="row">
                    {block name="right_column"}
                        <div id="right-column" class="col-xs-12 col-sm-4 col-md-3">
                            {if $page.page_name == 'product'}
                                {hook h='displayRightColumnProduct'}
                            {else}
                                {hook h="displayRightColumn"}
                            {/if}
                            {block name='sort_by'}
                                {include file='catalog/_partials/sort-orders.tpl' sort_orders=$listing.sort_orders}
                            {/block}
                        </div>
                    {/block}
                    <div class="col content-body">
                        {block name="content_wrapper"}
                            {hook h="displayContentWrapperTop"}
                            {block name="content"}
                                <p>Hello world! This is HTML5 Boilerplate.</p>
                            {/block}
                            {hook h="displayContentWrapperBottom"}
                        {/block}
                    </div>
                    {block name="left_column"}
                        <div id="left-column" class="col-xs-12 col-sm-4 col-md-3">
                            {if $page.page_name == 'product'}
                                {hook h='displayLeftColumnProduct'}
                            {else}
                                {hook h="displayLeftColumn"}
                            {/if}
                        </div>
                    {/block}
                </div>
            </div>
            {hook h="displayWrapperBottom"}
        </div>

    {/if}

    <footer id="footer">
        {block name="footer"}
            {include file="_partials/footer.tpl"}
        {/block}
    </footer>

</main>

{block name='javascript_bottom'}
    {include file="_partials/javascript.tpl" javascript=$javascript.bottom}
{/block}
<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

{block name='hook_before_body_closing_tag'}
    {hook h='displayBeforeBodyClosingTag'}
{/block}
</body>

</html>
