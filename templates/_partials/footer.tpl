<div class="top">
  <div class="container">
    <div class="row">
      <div class="col-md-4 links">
        <span class="title">راهنمای خرید</span>
        <ul>
          <li><a href="">ثبت سفارش</a></li>
          <li><a href="">پیگیری سفارش</a></li>
          <li><a href="">نحوه ارسال</a></li>
          <li><a href="">نحوه پرداخت</a></li>
        </ul>
      </div>
      <div class="col-md-4 links">
        <span class="title">راهنمای خرید</span>
        <ul>
          <li><a href="">ثبت سفارش</a></li>
          <li><a href="">پیگیری سفارش</a></li>
          <li><a href="">نحوه ارسال</a></li>
          <li><a href="">نحوه پرداخت</a></li>
        </ul>
      </div>
      <div class="col-md-4 links">
        <span class="title">راهنمای خرید</span>
        <ul>
          <li><a href="">ثبت سفارش</a></li>
          <li><a href="">پیگیری سفارش</a></li>
          <li><a href="">نحوه ارسال</a></li>
          <li><a href="">نحوه پرداخت</a></li>
        </ul>
      </div>
      <div class="col-md-4 download-box">
        <img src="{$urls.img_url}dl-app-store.png" alt="">
        <img src="{$urls.img_url}dl-google-play.png" alt="">
        <img src="{$urls.img_url}dl-direct-link.png" alt="">
        <img src="{$urls.img_url}dl-cafebazar.png" alt="">
      </div>
    </div>
  </div>
</div>
<div class="bottom">
  <div class="container">
    <div class="about-us">
      <div class="title">درباره IBUY</div>
      <div class="row">
        <div class="col-md-9">
          <div class="text">
            این روزها خریدهای اینترنتی حجم وسیعی از خریدهای روزانه ما را در بر می گیرد و در این میان انتخاب یک فروشگاه اینترنتی مطمئن و مناسب خیلی مهم است.
            فروشگاه اینترنتی آی بای در خدمت شماست تا بتوانید در فضایی آرام و مطمئن، بدون بیرون آمدن از منزل، کالاهای مصرفی و اساسی خانواده خود را تهیه کنید.
            در حال حاضر محصولات ما در 6 گروه چای، پروتئین، برنج، روغن، قند و شکر و حبوبات دسته بندی شده است که بالاترین کیفیت را دارا هستند و با تضمین کیفیت عرضه خواهند شد.
            آی بای با تنوع در محصولاتش که روز به روز هم به این تنوع افزوده خواهد شد، در تلاش است که تمام نیازهای شما را از خرید کالاهای پرمصرف و روزانه برطرف نماید.
            با ما در کمترین زمان ممکن سفارش خود را ثبت و با مناسب ترین قیمت تحویل بگیرید و یک خرید آسان، متفاوت و مطمئن را تجربه کنید.
          </div>
        </div>
        <div class="col-md-3">
          <a target="_blank" href="https://trustseal.enamad.ir/?id=154493&amp;Code=0ljAlPgO3lGpaiwwngUk"><img src="https://Trustseal.eNamad.ir/logo.aspx?id=154493&amp;Code=0ljAlPgO3lGpaiwwngUk" alt="" style="cursor:pointer" id="0ljAlPgO3lGpaiwwngUk"></a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="cp">کلیه حقوق برای IBuy محفوظ است</div>

{*<div class="container">*}
{*  <div class="row">*}
{*    {block name='hook_footer_before'}*}
{*      {hook h='displayFooterBefore'}*}
{*    {/block}*}
{*  </div>*}
{*</div>*}
{*<div class="footer-container">*}
{*  <div class="container">*}
{*    <div class="row">*}
{*      {block name='hook_footer'}*}
{*        {hook h='displayFooter'}*}
{*      {/block}*}
{*    </div>*}
{*    <div class="row">*}
{*      {block name='hook_footer_after'}*}
{*        {hook h='displayFooterAfter'}*}
{*      {/block}*}
{*    </div>*}
{*    <div class="row">*}
{*      <div class="col-md-12">*}
{*        <p class="text-sm-center">*}
{*          {block name='copyright_link'}*}
{*            <a class="_blank" href="https://www.prestashop.com" target="_blank" rel="nofollow">*}
{*              {l s='%copyright% %year% - Ecommerce software by %prestashop%' sprintf=['%prestashop%' => 'PrestaShop™', '%year%' => 'Y'|date, '%copyright%' => '©'] d='Shop.Theme.Global'}*}
{*            </a>*}
{*          {/block}*}
{*        </p>*}
{*      </div>*}
{*    </div>*}
{*  </div>*}
{*</div>*}
