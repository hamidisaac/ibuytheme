<div class="banners-box">
    <div class="container">
        {hook h='displayBannerAndSlider'}
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="row">
            <div class="col-auto cat-box">
                {hook h='displayHomeCategory'}
            </div>
            <div class="col section">
                {assign var='HOOK_HOME_TAB_CONTENT' value=Hook::exec('displayHomeTabContent')}
                {assign var='HOOK_HOME_TAB' value=Hook::exec('displayHomeTab')}

                {if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
                <div class="navbar-top">
                    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
                        <ul class="nav">
                            {$HOOK_HOME_TAB nofilter}
                        </ul>
                    {/if}
                </div>
                <div class="tabs" id="myTabContent">
                    {$HOOK_HOME_TAB_CONTENT nofilter}
                </div>
                {/if}

                <div class="features">
                    <div class="feature-box">
                        <div class="image"></div>
                        <div class="title">مناسب ترین قیمت</div>
                    </div>
                    <div class="feature-box">
                        <div class="image"></div>
                        <div class="title">پشتیبانی 24 ساعته</div>
                    </div>
                    <div class="feature-box">
                        <div class="image"></div>
                        <div class="title">ارسال در کوتاه ترین زمان</div>
                    </div>
                    <div class="feature-box">
                        <div class="image"></div>
                        <div class="title">خرید آسان</div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>