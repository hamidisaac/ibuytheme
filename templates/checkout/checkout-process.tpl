<div class="order-steps">
    <div class="steps-box">
        {foreach from=$steps2 item="step" key="index"}
            <span data-title="{$step.title}"
                  class="step-details {($step.current == 1)? 'current' : ''}  {($step.complete == 1)? 'complete' : ''}"
            >
            </span>

        {/foreach}
    </div>

    {foreach from=$steps item="step" key="index"}
        {render identifier  =  $step.identifier
        position    =  ($index + 1)
        ui          =  $step.ui
        }
    {/foreach}
</div>
