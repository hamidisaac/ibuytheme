
{extends file='customer/_partials/login-form.tpl'}

{block "form_fields"}
  <div class="send-sms-box"  >
    {block "form_field"}
      {if isset($formFields['phone']) }
        {form_field field=$formFields['phone']}
      {/if}
    {/block}
    <div class="note">برای ورود شماره موبایل خود را وارد نمایید، یک کد 5 رقمی از طریق پیامک برای شما ارسال خواهد شد</div>
  </div>
  <div class="verification-box" style="display: none">
    {foreach from=$formFields item="field"}
      <div id="form_input_{$field['name']}">
        {block "form_field"}
          {if  $field['name'] !== 'phone'}
            {form_field field=$field }
          {/if}
        {/block}
      </div>
    {/foreach}
  </div>
{/block}


{block name='form_buttons'}
  <button
    class="continue btn btn-primary float-xs-right send-sms-btn"
    name="continue"
    data-link-action="sign-in"
    type="submit"
    value="1"
  >
    {l s='Continue' d='Shop.Theme.Actions'}
  </button>
{/block}
