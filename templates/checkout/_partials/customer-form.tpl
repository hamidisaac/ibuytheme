
{extends "customer/_partials/customer-form.tpl"}

{block "form_fields"}
    <div class="send-sms-box">
        {block "form_field"}
            {if isset($formFields['phone']) }
                {form_field field=$formFields['phone']}
            {/if}
        {/block}
    </div>
    <div class="verification-box" style="display: none">
        {foreach from=$formFields item="field"}
            <div id="form_input_{$field['name']}">
                {block "form_field"}
                    {if  $field['name'] !== 'phone'}
                        {form_field field=$field}
                    {/if}
                {/block}
            </div>
        {/foreach}
    </div>

    {$hook_create_account_form nofilter}
{/block}

{block "form_buttons"}
    <button
      class="continue btn btn-primary float-xs-right send-sms-btn"
      name="continue"
      data-link-action="register-new-customer"
      type="submit"
      value="1"
    >
        {l s='Continue' d='Shop.Theme.Actions'}
    </button>
{/block}
