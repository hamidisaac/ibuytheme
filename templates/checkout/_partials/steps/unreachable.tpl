{block name='step'}
  <section class="checkout-step -unreachable" id="{$identifier}">
    <h1 class="step-title h3">
      {$title}
    </h1>
  </section>
{/block}
