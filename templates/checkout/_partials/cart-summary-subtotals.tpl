
  {foreach from=$cart.subtotals item="subtotal"}
    {if $subtotal.value && $subtotal.type !== 'tax'}
      <div class="item" id="cart-subtotal-{$subtotal.type}">
        <span class="name">
            {$subtotal.label}
        </span>

        <span class="value">
          {$subtotal.value}
        </span>
      </div>
    {/if}
  {/foreach}


