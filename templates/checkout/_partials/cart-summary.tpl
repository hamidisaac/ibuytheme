<section id="js-checkout-summary" class=" js-cart" data-refresh-url="{$urls.pages.cart}?ajax=1&action=refresh">
    <div class="checkout-sunmmary">
        <div class="header">
            <h3>خلاصه سفارش</h3>
        </div>
        <div class="body">
            {block name='hook_checkout_summary_top'}
                {hook h='displayCheckoutSummaryTop'}
            {/block}

            {block name='cart_summary_products'}
                <div class="cart-summary-products">
                    {block name='cart_summary_product_list'}
                        <div class="checkout-sunmmary-products" id="cart-summary-product-list">
                            {foreach from=$cart.products item=product}
                                <div class="product-item">
                                    {include file='checkout/_partials/cart-summary-product-line.tpl' product=$product}
                                </div>
                            {/foreach}
                        </div>
                    {/block}
                </div>
            {/block}

        </div>
        <div class="footer">
            {block name='cart_summary_subtotals'}
                {include file='checkout/_partials/cart-summary-subtotals.tpl' cart=$cart}
            {/block}

            {block name='cart_summary_totals'}
                {include file='checkout/_partials/cart-summary-totals.tpl' cart=$cart}
            {/block}

            {block name='cart_summary_voucher'}
{*                {include file='checkout/_partials/cart-voucher.tpl'}*}
            {/block}

        </div>
    </div>
</section>
