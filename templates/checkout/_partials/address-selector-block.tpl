{block name='address_selector_blocks'}
  {foreach $addresses as $address}
<article
        class="address-item{if $address.id == $selected} selected{/if}"
        id="{$name|classname}-address-{$address.id}"
>
    <label class="address-item-radio-box custom-control custom-radio" for="address-item-{$address.id}">
        <input type="radio" class="custom-control-input" {if $address.id == $selected} checked{/if} id="address-item-{$address.id}" name="defaultExampleRadios">
        <label class="custom-control-label" for="address-item-{$address.id}"> </label>
    </label>
    <label for="address-item-{$address.id}" class="address-item-body">
        <strong>{$address.alias}</strong>
        {$address.city nofilter}, {$address.address1 nofilter}
    </label>
    <div class="address-item-options">
        {if $interactive}
            <a
                    class="edit-address "
                    data-link-action="edit-address"
                    href="{url entity='order' params=['id_address' => $address.id, 'editAddress' => $type, 'token' => $token]}"
            >
                {l s='Edit' d='Shop.Theme.Actions'}
            </a>
            <a
                    class="delete-address "
                    data-link-action="delete-address"
                    href="{url entity='order' params=['id_address' => $address.id, 'deleteAddress' => true, 'token' => $token]}"
            >
                {l s='Delete' d='Shop.Theme.Actions'}
            </a>
        {/if}
    </div>
</article>

  {/foreach}
  {if $interactive}
    <p>
      <button class="ps-hidden-by-js form-control-submit center-block" type="submit">{l s='Save' d='Shop.Theme.Actions'}</button>
    </p>
  {/if}
{/block}
