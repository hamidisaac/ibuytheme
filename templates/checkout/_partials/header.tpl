{block name='header'}
  {block name='header_nav'}
    <nav class="order-header">
      <div class="container">
        <a href="{$urls.base_url}">
          <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name} {l s='logo' d='Shop.Theme.Global'}">
        </a>
      </div>
    </nav>
  {/block}

{/block}
