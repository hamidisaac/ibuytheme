<div class="item-image">
  <img src="{$product.cover.bySize.cart_default.url}" alt="{$product.name|escape:'quotes'}">
</div>
<div class="item-content">
  <h3 class="product-name"><a class="label" href="{$product.url}" data-id_customization="{$product.id_customization|intval}">{$product.name}</a><span class="description">(100 گرم)</span></h3>
  <div class="product-price">{$product.price}</div>
</div>
<div class="quantity">
    {if isset($product.is_gift) && $product.is_gift}
        <span class="gift-quantity">{$product.quantity}</span>
    {else}
        <button class="quantity-add js-increase-product-quantity" onclick="this.parentNode.querySelector('input[type=number]').stepUp();$(this).parent('.quantity').find('input').trigger('touchspin.on.startupspin')" >
            <ion-icon name="add-outline" class="add-outline"></ion-icon>
        </button>
        <input
                class="js-cart-line-product-quantity"
                data-down-url="{$product.down_quantity_url}"
                data-up-url="{$product.up_quantity_url}"
                data-update-url="{$product.update_quantity_url}"
                data-product-id="{$product.id_product}"
                type="number"
                value="{$product.quantity}"
                name="product-quantity-spin"
                min="{$product.minimal_quantity}"
        />
        <button class="quantity-remove js-decrease-product-quantity" onclick="this.parentNode.querySelector('input[type=number]').stepDown();$(this).parent('.quantity').find('input').trigger('touchspin.on.startdownspin')">
            <ion-icon name="remove-outline" class="remove-outline"></ion-icon>
        </button>
    {/if}
</div>
<div class="item-remove">
    <a
            class                       = "remove-from-cart"
            rel                         = "nofollow"
            href                        = "{$product.remove_from_cart_url}"
            data-link-action            = "delete-from-cart"
            data-id-product             = "{$product.id_product|escape:'javascript'}"
            data-id-product-attribute   = "{$product.id_product_attribute|escape:'javascript'}"
            data-id-customization   	  = "{$product.id_customization|escape:'javascript'}"
    >
        {if !isset($product.is_gift) || !$product.is_gift}
            <ion-icon name="trash-outline"></ion-icon>
        {/if}
    </a>

    {block name='hook_cart_extra_product_actions'}
        {hook h='displayCartExtraProductActions' product=$product}
    {/block}

</div>


