{extends file=$layout}

{block name='head_seo' prepend}
    <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
    <meta property="og:type" content="product">
    <meta property="og:url" content="{$urls.current_url}">
    <meta property="og:title" content="{$page.meta.title}">
    <meta property="og:site_name" content="{$shop.name}">
    <meta property="og:description" content="{$page.meta.description}">
    <meta property="og:image" content="{$product.cover.large.url}">
    {if $product.show_price}
        <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
        <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
        <meta property="product:price:amount" content="{$product.price_amount}">
        <meta property="product:price:currency" content="{$currency.iso_code}">
    {/if}
    {if isset($product.weight) && ($product.weight != 0)}
        <meta property="product:weight:value" content="{$product.weight}">
        <meta property="product:weight:units" content="{$product.weight_unit}">
    {/if}
{/block}

{block name='content'}
    <section id="main" itemscope itemtype="https://schema.org/Product">
        <meta itemprop="url" content="{$product.url}">
        <div class="product">
            <div class="row product-details">
                <div class="col product-description">
                        {block name='page_header_container'}
                            {block name='page_header'}
                                <h1 class="product-title" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                            {/block}
                        {/block}
                    {block name='product_description_short'}
                        <div id="description" itemprop="description">{$product.description_short nofilter}</div>
                    {/block}
                    <div class="specification">
                        <div class="item">
                            <span class="name">نوع</span>
                            <span class="value">گوساله</span>
                        </div>
                        <div class="item">
                            <span class="name">نوع</span>
                            <span class="value">گوساله</span>
                        </div>
                        <div class="item">
                            <span class="name">نوع</span>
                            <span class="value">گوساله</span>
                        </div>
                        <div class="item">
                            <span class="name">نوع</span>
                            <span class="value">گوساله</span>
                        </div>
                    </div>

                </div>
                <div class="col-auto product-image">
                    <div class="image">
                        {block name='product_cover_thumbnails'}
                            {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                        {/block}
                    </div>
                </div>
                <div class="col-auto product-actions">
                    <div class="tools">
                        <a class="tool-item" href="">
                            <ion-icon name="heart"></ion-icon>
                        </a>
                        <a class="tool-item" href="">
                            <ion-icon name="share-social-sharp"></ion-icon>
                        </a>
                    </div>
                </div>
            </div>
            <div class="price-box">
                <div class="row">
                    <div class="col">
                        <div class="title">قیمت</div>
                        <div class="price">{$product.price}</div>
                    </div>
                    <div class="col-auto">
                        {block name='product_buy'}
                            <div class="product-actions">
                                <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                    <input type="hidden" name="token" value="{$static_token}">
                                    <input type="hidden" name="id_product" value="{$product.id}"
                                           id="product_page_product_id">
                                    <input type="hidden" name="id_customization" value="{$product.id_customization}"
                                           id="product_customization_id">

                                    {block name='product_add_to_cart'}
                                        {include file='catalog/_partials/product-add-to-cart.tpl'}
                                    {/block}

                                    {block name='product_refresh'}{/block}
                                </form>
                            </div>
                        {/block}
                    </div>
                </div>
            </div>
        </div>

        {block name='product_footer'}
            {hook h='displayFooterProduct' product=$product category=$category}
        {/block}

    </section>
{/block}