
{extends file=$layout}

{block name='content'}
    <section id="main">


        <section id="products">
            {if $listing.products|count}
                {block name='product_list'}
                    {include file='catalog/_partials/products.tpl' listing=$listing}
                {/block}
            {else}

                <div id="js-product-list">
                    {include file='errors/not-found.tpl'}
                </div>

            {/if}
        </section>

    </section>
{/block}
