<div class="sort-box">
    <h2 class="title">مرتب سازی بر اساس</h2>
    <ul>
        {$listing.sort_selected}
        {foreach from=$listing.sort_orders item=sort_order}
            <li>
                <a
                        onclick="$('.radio').removeClass('active');$(this).find('.radio').addClass('active')"
                        rel="nofollow"
                        href="{$sort_order.url}"
                        class="select-list {['current' => $sort_order.current, 'js-search-link' => true]|classnames}"
                >
                <span class="radio {if (isset($listing.sort_selected) && $listing.sort_selected == $sort_order.label)}active{/if}"></span>
                {$sort_order.label}
            </a></li>
        {/foreach}
    </ul>
</div>


