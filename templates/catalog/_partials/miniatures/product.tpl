
{block name='product_miniature_item'}
    <article class="product-item js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
        {block name='product_flags'}
            <ul class="product-flags">
                {foreach from=$product.flags item=flag}
                    <li class="product-flag {$flag.type}">{$flag.label}</li>
                {/foreach}
            </ul>
        {/block}
        <div class="image">
            {block name='product_thumbnail'}
                {if $product.cover}
                    <a href="{$product.url}" class="thumbnail product-thumbnail">
                        <img
                                src="{$product.cover.bySize.home_default.url}"
                                alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
                                data-full-size-image-url="{$product.cover.large.url}"
                        />
                    </a>
                {else}
                    <a href="{$product.url}" class="thumbnail product-thumbnail">
                        <img src="{$urls.no_picture_image.bySize.home_default.url}" />
                    </a>
                {/if}
            {/block}
        </div>
        <h2 class="title">
            {block name='product_name'}
                {if $page.page_name == 'index'}
                    <h3 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:15:'...'}</a></h3>
                {else}
                    <h2 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:15:'...'}</a></h2>
                {/if}
            {/block}
        </h2>
        <div class="description" data-action="quickShow">10 گرم</div>
        <div class="tool-box">

            {block name='product_price_and_shipping'}
                {if $product.show_price}
                    <div class="product-price-and-shipping">
                        {hook h='displayProductPriceBlock' product=$product type="before_price"}

                        <div itemprop="price" class="price">{$product.price}</div>
                        {hook h='displayProductPriceBlock' product=$product type='unit_price'}

                        {hook h='displayProductPriceBlock' product=$product type='weight'}
                    </div>
                {/if}
            {/block}

            {block name='quick_view'}
                <a class="btn-buy quick-view"  href="#" data-link-action="quickview">
{*                    {l s='Quick view' d='Shop.Theme.Actions'}*}
                    خرید
                </a>
            {/block}
        </div>
    </article>
{/block}
