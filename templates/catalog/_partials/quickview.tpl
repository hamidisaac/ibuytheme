<div id="quickview-modal-{$product.id}-{$product.id_product_attribute}" class="modal fade quickview" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="product-title">{$product.name}</div>
                        <div class="specification">
                            <div class="item">
                                <span class="name">نوع</span>
                                <span class="value">گوساله</span>
                            </div>
                            <div class="item">
                                <span class="name">نوع</span>
                                <span class="value">گوساله</span>
                            </div>
                            <div class="item">
                                <span class="name">نوع</span>
                                <span class="value">گوساله</span>
                            </div>
                            <div class="item">
                                <span class="name">نوع</span>
                                <span class="value">گوساله</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="image">
                            {block name='product_cover_thumbnails'}
                                {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                            {/block}
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="tools">
                            <a class="tool-item" href="">
                                <ion-icon name="heart"></ion-icon>
                            </a>
                            <a class="tool-item" href="">
                                <ion-icon name="share-social-sharp"></ion-icon>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col product-description">
                        <div class="title">ویژگی های محصول</div>
                        {block name='product_description_short'}
                            <div id="description" itemprop="description">{$product.description_short nofilter}</div>
                        {/block}
                    </div>
                </div>
            </div>

            <div class="price-box">
                <div class="row">
                    <div class="col">
                        <div class="title">قیمت</div>
                        <div class="price">{$product.price}</div>
                    </div>
                    <div class="col-auto">
                        {block name='product_buy'}
                            <div class="product-actions">
                                <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                    <input type="hidden" name="token" value="{$static_token}">
                                    <input type="hidden" name="id_product" value="{$product.id}"
                                           id="product_page_product_id">
                                    <input type="hidden" name="id_customization" value="{$product.id_customization}"
                                           id="product_customization_id">

                                    {block name='product_add_to_cart'}
                                        {include file='catalog/_partials/product-add-to-cart.tpl'}
                                    {/block}

                                    {block name='product_refresh'}{/block}
                                </form>
                            </div>
                        {/block}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {hook h='displayProductAdditionalInfo' product=$product  }
            </div>
        </div>
    </div>
</div>



