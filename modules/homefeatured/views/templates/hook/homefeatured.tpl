
<section class="featured-products tab-pane active" id="homefeatured">
    <div class="products">
        {foreach from=$products item="product" key="key"}
            {include file="catalog/_partials/miniatures/product.tpl" product=$product}
        {/foreach}
    </div>
</section>
