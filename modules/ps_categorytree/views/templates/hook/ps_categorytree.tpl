{if $page.page_name == 'index'}
    <h3 class="title">دسته بندی</h3>
<ul class="categories">
{else}
    <ul class="categories row">
{/if}


    {foreach from=$categories.children item=category}
        {assign var="cat_url" value="{_PS_IMG_}c/{$category.id}.jpg"}
        {assign var="cat_bg" value="{_PS_IMG_}c/{$category.id}_thumb.jpg"}
        <li>
            <a class="cat-item" href="{$category.link}" style="background-image: url({$cat_bg})">
                <span>{$category.name}</span>
                <i class="cat-icon-1" style="background-image: url({$cat_url})"></i>
            </a>
        </li>
    {/foreach}
</ul>


