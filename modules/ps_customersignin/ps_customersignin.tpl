{if $logged}
{*    <a*}
{*            class="logout hidden-sm-down"*}
{*            href="{$logout_url}"*}
{*            rel="nofollow"*}
{*    >*}
{*        <i class="material-icons">&#xE7FF;</i>*}
{*        {l s='Sign out' d='Shop.Theme.Actions'}*}
{*    </a>*}
{*    <a*}
{*            class="account"*}
{*            href="{$my_account_url}"*}
{*            title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}"*}
{*            rel="nofollow"*}
{*    >*}
{*        <i class="material-icons hidden-md-up logged">&#xE7FF;</i>*}
{*        <span class="hidden-sm-down">{$customerName}</span>*}
{*    </a>*}
    <li>
        <a
                href="{$my_account_url}"
                rel="nofollow"
                title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
        >
            <span class="icon-user"><span class="path1"></span><span class="path2"></span></span>
        </a>
    </li>
{else}
    <li>
        <a
                href="{$my_account_url}"
                rel="nofollow"
                title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
        >
            <span class="icon-user"><span class="path1"></span><span class="path2"></span></span>
        </a>
    </li>
{/if}
