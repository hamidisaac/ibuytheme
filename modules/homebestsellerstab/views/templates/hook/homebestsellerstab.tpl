
<section class="featured-products tab-pane fade" id="homebestsellerstab">
    <div class="products">
        {if $products}
            {foreach from=$products item="product"}
                {include file="catalog/_partials/miniatures/product.tpl" product=$product}
            {/foreach}
        {else}
            <div class="col-md-12">
                <div class="alert alert-warning">
                    {l s='No new products found' mod='homebestsellers'}
                </div>
            </div>
        {/if}
    </div>
</section>
