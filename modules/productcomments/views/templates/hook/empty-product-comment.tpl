<div id="empty-product-comment" class="product-comment-list-item">
  {if $post_allowed}
    <button class="btn btn-comment btn-comment-big post-product-comment">
      <i class="material-icons shopping-cart">edit</i>
      {l s='Be the first to write your review' d='Modules.Productcomments.Shop'}
    </button>
  {else}
    {l s='No customer reviews for the moment.' d='Modules.Productcomments.Shop'}
  {/if}
</div>
