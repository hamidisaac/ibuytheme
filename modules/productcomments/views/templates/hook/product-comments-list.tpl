<div class="product-comments">
    <script type="text/javascript">
        var productCommentUpdatePostErrorMessage = '{l s='Sorry, your review appreciation cannot be sent.' d='Modules.Productcomments.Shop' js=1}';
        var productCommentAbuseReportErrorMessage = '{l s='Sorry, your abuse report cannot be sent.' d='Modules.Productcomments.Shop' js=1}';
    </script>
    {include file='module:productcomments/views/templates/hook/product-comment-item-prototype.tpl' assign="comment_prototype"}
    {include file='module:productcomments/views/templates/hook/empty-product-comment.tpl'}

    <div class="header">
        <h3 class="title">نظرات</h3>
        <a class="new-comment" href="#product-comments-list">نظر خود را وارد کنید</a>
    </div>
    <div class="comments"
         id="product-comments-list"
         data-list-comments-url="{$list_comments_url nofilter}"
         data-update-comment-usefulness-url="{$update_comment_usefulness_url nofilter}"
         data-report-comment-url="{$report_comment_url nofilter}"
         data-comment-item-prototype="{$comment_prototype|escape:'html_attr'}">
    </div>
    <div id="product-comments-list-pagination"></div>
</div>


{* Appreciation post error modal *}
{include file='module:productcomments/views/templates/hook/alert-modal.tpl'
modal_id='update-comment-usefulness-post-error'
modal_title={l s='Your review appreciation cannot be sent' d='Modules.Productcomments.Shop'}
icon='error'
}

{* Confirm report modal *}
{include file='module:productcomments/views/templates/hook/confirm-modal.tpl'
modal_id='report-comment-confirmation'
modal_title={l s='Report comment' d='Modules.Productcomments.Shop'}
modal_message={l s='Are you sure that you want to report this comment?' d='Modules.Productcomments.Shop'}
icon='feedback'
}

{* Report comment posted modal *}
{include file='module:productcomments/views/templates/hook/alert-modal.tpl'
modal_id='report-comment-posted'
modal_title={l s='Report sent' d='Modules.Productcomments.Shop'}
modal_message={l s='Your report has been submitted and will be considered by a moderator.' d='Modules.Productcomments.Shop'}
}

{* Report abuse error modal *}
{include file='module:productcomments/views/templates/hook/alert-modal.tpl'
modal_id='report-comment-post-error'
modal_title={l s='Your report cannot be sent' d='Modules.Productcomments.Shop'}
icon='error'
}
