
<script type="text/javascript">
  var productCommentPostErrorMessage = '{l s='Sorry, your review cannot be posted.' d='Modules.Productcomments.Shop' js=1}';
</script>

<div class="new-comment-box" id="post-product-comment-box" >
  <div class="title">ثبت نظر</div>

    <form class="comment-form" id="post-product-comment-form" action="{$post_comment_url nofilter}" method="POST">
        <div class="stars">
          {if $criterions|@count > 0}
            <ul id="criterions_list">
              {foreach from=$criterions item='criterion'}
                <li>
                  <div class="criterion-rating">
                    <div
                        class="grade-stars"
                        data-grade="3"
                        data-input="criterion[{$criterion.id_product_comment_criterion}]">
                    </div>
                  </div>
                </li>
              {/foreach}
            </ul>
          {/if}
        </div>
        <div class="create-box">

            <div class="comment">
                <ion-icon class='icon' name="pencil"></ion-icon>
                <input type="hidden" name="comment_title" value="ss">
                <textarea name="comment_content"></textarea>
            </div>
        </div>
        <button type="submit" class="btn btn-comment btn-comment-big">
            ارسال
        </button>
    </form>
</div>

{* Comment posted modal *}
{if $moderation_active}
  {assign var='comment_posted_message' value={l s='Your comment has been submitted and will be available once approved by a moderator.' d='Modules.Productcomments.Shop'}}
{else}
  {assign var='comment_posted_message' value={l s='Your comment has been added!' d='Modules.Productcomments.Shop'}}
{/if}
{include file='module:productcomments/views/templates/hook/alert-modal.tpl'
modal_id='product-comment-posted-modal'
modal_title={l s='Review sent' d='Modules.Productcomments.Shop'}
modal_message=$comment_posted_message
}

{* Comment post error modal *}
{include file='module:productcomments/views/templates/hook/alert-modal.tpl'
modal_id='product-comment-post-error'
modal_title={l s='Your review cannot be sent' d='Modules.Productcomments.Shop'}
icon='error'
}
