<div class="item" data-product-comment-id="@COMMENT_ID@" data-product-id="@PRODUCT_ID@">
    <div class="image">
        <img src="" alt="">
    </div>
    <div class="content">
        <div class="header">
            <span class="title">{l s='%1$s' sprintf=['@CUSTOMER_NAME@'] d='Modules.Productcomments.Shop'}</span>
            <span class="stars"><div class="grade-stars"></div></span>
        </div>
        <div class="body">@COMMENT_COMMENT@</div>
    </div>
</div>
