{if $nb_comments != 0}
  <div class="comments-note">
    <span>{l s='Grade' d='Modules.Productcomments.Shop'}</span>
    <div class="grade-stars" data-grade="{$grade}"></div>
  </div>
{/if}
