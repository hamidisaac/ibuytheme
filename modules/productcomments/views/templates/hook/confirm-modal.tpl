{assign var='icon' value=$icon|default:'check_circle'}
{assign var='modal_message' value=$modal_message|default:''}

<script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function() {
    const confirmModal = $('#{$modal_id}');
    confirmModal.on('hidden.bs.modal', function () {
      confirmModal.modal('hide');
      confirmModal.trigger('modal:confirm', false);
    });

    $('.confirm-button', confirmModal).click(function() {
      confirmModal.trigger('modal:confirm', true);
    });
    $('.refuse-button', confirmModal).click(function() {
      confirmModal.trigger('modal:confirm', false);
    });
  });
</script>

<div id="{$modal_id}" class="modal fade product-comment-modal" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2>
          <i class="material-icons {$icon}">{$icon}</i>
          {$modal_title}
        </h2>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12  col-sm-12" id="{$modal_id}-message">
            {$modal_message}
          </div>
        </div>
        <div class="row">
          <div class="col-md-12  col-sm-12 post-comment-buttons">
            <button type="button" class="btn btn-comment-inverse btn-comment-huge refuse-button" data-dismiss="modal" aria-label="{l s='No' d='Modules.Productcomments.Shop'}">
              {l s='No' d='Modules.Productcomments.Shop'}
            </button>
            <button type="button" class="btn btn-comment btn-comment-huge confirm-button" data-dismiss="modal" aria-label="{l s='Yes' d='Modules.Productcomments.Shop'}">
              {l s='Yes' d='Modules.Productcomments.Shop'}
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
