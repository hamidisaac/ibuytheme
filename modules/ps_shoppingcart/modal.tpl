<div id="blockcart-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="sidebar" >
    <div class="sidebar-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <ion-icon name="arrow-forward-outline"></ion-icon>
      </button>
      <span class="title">سبد خرید شما</span>
    </div>
    <div class="sidebar-body">
      <div class="cart-overview js-cart" data-refresh-url="{url entity='cart' params=['ajax' => true, 'action' => 'refresh']}" >
        <ul class="cart-items">
          {foreach from=$cart.products item=product}
            <li class="item">
              {block name='cart_detailed_product_line'}
                {include file='checkout/_partials/cart-detailed-product-line.tpl' product=$product}
              {/block}
            </li>
          {/foreach}
        </ul>
      </div>
    </div>
    <div class="sidebar-footer">
      <div class="cart-information cart-detailed-totals">
        <div class="item">
          <span class="name">جمع کالاها</span>
          <span class="value">{$cart.subtotals.products.value}</span>
        </div>
        <div class="item bold">
          <span class="name">سود شما از این خرید</span>
          <span class="value">{$cart.totals.total.value}</span>
        </div>
        <div class="item bolder">
          <span class="name">مبلغ قابل پرداخت</span>
          <span class="value green">{$cart.subtotals.tax.label}</span>
        </div>
      </div>
      <div class="cart-print">
        <ion-icon name="print-outline"></ion-icon>
        مشاهده به صورت فاکتور</div>
      <div class="cart-buttons ">
        <a href="{$cart_url}&checkout=1" class="btn-buy">پرداخت نهایی</a>

        <a href="" class="btn-remove" style="display: none">
          <ion-icon name="trash-outline"></ion-icon>
        </a>
      </div>
    </div>
  </div>

</div>
