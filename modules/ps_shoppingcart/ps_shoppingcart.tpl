<li>
    <div id="_desktop_cart">
        <div class="blockcart cart-preview" data-refresh-url="{$refresh_url}">
            {if $cart.products_count > 0}
                <a class="show-cart-model" rel="nofollow" hrefs="{$cart_url}" >
            {/if}
                <span class="icon-cart"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></span>
            {if $cart.products_count > 0}
                <span class="item-count">{$cart.products_count}</span>
            </a>
            {/if}
            {literal}
                <script>
                    if(typeof $ !== "undefined"){
                        prestashop.blockcart = prestashop.blockcart || {};

                        var showModal = prestashop.blockcart.showModal || function (modal) {

                            var $body = $('body');
                            $body.append(modal);
                            $body.one('click', '#blockcart-modal', function (event) {
                                if (event.target.id === 'blockcart-modal') {
                                    $(event.target).remove();
                                }
                            });
                        };
                        $('.show-cart-model').click(()=>{
                            var refreshURL = $('.blockcart').data('refresh-url');
                            var requestData = {};

                            requestData = {
                                id_product_attribute: 1,
                                id_product: 1,
                                action: 'add-to-cart'
                            };

                            $.post(refreshURL, requestData).then(function (resp) {
                                showModal(resp.modal);
                            }).fail(function (resp) {
                                prestashop.emit('handleError', {eventType: 'updateShoppingCart', resp: resp});
                            });
                        })

                    }

                </script>
            {/literal}
        </div>
    </div>
</li>

{*<div id="_desktop_cart">*}
{*    <div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" >*}
{*        <div class="header">*}
{*            {if $cart.products_count > 0}*}
{*            <a rel="nofollow" href="{$cart_url}">*}
{*                {/if}*}
{*                <i class="material-icons shopping-cart">shopping_cart</i>*}
{*                <span class="hidden-sm-down">{l s='Cart' d='Shop.Theme.Checkout'}</span>*}
{*                <span class="cart-products-count">({$cart.products_count})</span>*}
{*                {if $cart.products_count > 0}*}
{*            </a>*}
{*            {/if}*}
{*        </div>*}
{*    </div>*}
{*</div>*}